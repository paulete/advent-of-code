"""
--- Part Two ---

Your calculation isn't quite right. It looks like some of the digits are actually spelled out with
letters: one, two, three, four, five, six, seven, eight, and nine also count as valid "digits".


Equipped with this new information, you now need to find the real first and last digit on each line.

What is the sum of all of the calibration values?
"""

NUMBERS = ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"]
N_MAP = {number: str(n) for n, number in enumerate(NUMBERS, 1)}


def solve(_input: str) -> int:
    def replace_num(line: str) -> str:
        for number in sorted(NUMBERS, key=line.find):
            line = line.replace(number, N_MAP[number])
        return line

    lines = [replace_num(line) for line in _input.splitlines()]
    vals = ["".join(c for c in line if c.isdigit()) for line in lines]
    return sum(int(val[0] + val[-1]) for val in vals)
