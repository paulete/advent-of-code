import importlib
import json
from typing import Dict


def run_day(day: int) -> None:
    try:
        part_one = importlib.import_module(f"day_{day:02d}.part_one")
        part_two = importlib.import_module(f"day_{day:02d}.part_two")

        with open(f"day_{day:02d}/input.txt", "r") as f:
            my_input = f.read()
            print(f"Day {day} - Part One: {part_one.solve(my_input)}")
            print(f"Day {day} - Part Two: {part_two.solve(my_input)}\n")

    except ModuleNotFoundError:
        print(f"Solutions for day {day} are not yet available.\n")
    except Exception as e:
        print(f"Error runnig solution for day {day}: {e}\n")


def load_json(filepath: str) -> Dict:
    with open(filepath, "r") as f:
        return json.load(f)
