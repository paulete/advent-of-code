import sys

from utils.helpers import run_day


def main() -> None:
    if len(sys.argv) > 1:
        for arg in sys.argv[1:]:
            try:
                day = int(arg)
                if 1 <= day <= 25:
                    run_day(day)
                else:
                    print(f"Day {day} is out of range...")
            except ValueError:
                print(f"{arg} is not a valid parameter...")
    else:
        for day in range(1, 26):
            run_day(day)


if __name__ == "__main__":
    main()
