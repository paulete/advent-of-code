import pytest
from typing import Dict

from day_02 import part_one, part_two
from utils.helpers import load_json

test_cases = load_json("day_02/tests.json")


@pytest.mark.parametrize("test_case", test_cases["part_one"])
def test_part_one(test_case: Dict) -> None:
    expected = test_case["output"]
    implemented = part_one.solve(test_case["input"])
    assert expected == implemented, f"Expected {expected}, got {implemented}"


@pytest.mark.parametrize("test_case", test_cases["part_two"])
def test_part_two(test_case: Dict) -> None:
    expected = test_case["output"]
    implemented = part_two.solve(test_case["input"])
    assert expected == implemented, f"Expected {expected}, got {implemented}"
