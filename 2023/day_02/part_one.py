"""
--- Day 2: Cube Conundrum ---

You're launched high into the atmosphere! The apex of your trajectory just barely reaches the surface
of a large island floating in the sky. You gently land in a fluffy pile of leaves. It's quite cold,
but you don't see much snow. An Elf runs over to greet you.

The Elf explains that you've arrived at Snow Island and apologizes for the lack of snow. He'll be
happy to explain the situation, but it's a bit of a walk, so you have some time. They don't get many
visitors up here; would you like to play a game in the meantime?

As you walk, the Elf shows you a small bag and some cubes which are either red, green, or blue. Each
time you play this game, he will hide a secret number of cubes of each color in the bag, and your 
goal is to figure out information about the number of cubes.

To get information, once a bag has been loaded with cubes, the Elf will reach into the bag, grab a
handful of random cubes, show them to you, and then put them back in the bag. He'll do this a few
times per game.

You play several games and record the information from each game (your puzzle input). Each game is
listed with its ID number (like the 11 in Game 11: ...) followed by a semicolon-separated list of
subsets of cubes that were revealed from the bag (like 3 red, 5 green, 4 blue).

Determine which games would have been possible if the bag had been loaded with only 12 red cubes, 13
green cubes, and 14 blue cubes. What is the sum of the IDs of those games?
"""


def solve(_input: str) -> int:
    def simplify_line(line: str) -> str:
        return line.replace(" ", "")[line.find(":") :]

    def is_possible(subset: str) -> bool:
        record = {"red": 0, "green": 0, "blue": 0}
        for reveal in subset.split(","):
            count = "".join(char for char in reveal if char.isdigit())
            color = reveal.replace(count, "")
            record[color] += int(count)
        return record["red"] <= 12 and record["green"] <= 13 and record["blue"] <= 14

    lines = _input.splitlines()
    return sum(
        i
        for i, line in enumerate(lines, 1)
        if all(is_possible(subset) for subset in simplify_line(line).split(";"))
    )
