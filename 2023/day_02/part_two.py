"""
--- Part Two ---

The Elf says they've stopped producing snow because they aren't getting any water! He isn't sure why
the water stopped; however, he can show you how to get to the water source to check it out for
yourself. It's just up ahead!

As you continue your walk, the Elf poses a second question: in each game you played, what is the
fewest number of cubes of each color that could have been in the bag to make the game possible?

The power of a set of cubes is equal to the numbers of red, green, and blue cubes multiplied
together.

For each game, find the minimum set of cubes that must have been present. What is the sum of the
power of these sets?
"""

import operator
from functools import reduce


def solve(_input: str) -> int:
    def simplify_line(line: str) -> str:
        return line.replace(" ", "")[line.find(":") :]

    def get_set_power(game: str) -> int:
        record = {"red": 0, "green": 0, "blue": 0}
        for reveal in game.replace(";", ",").split(","):
            count = "".join(char for char in reveal if char.isdigit())
            color = reveal.replace(count, "")
            record[color] = max(record[color], int(count))
        return reduce(operator.mul, record.values())

    return sum(get_set_power(simplify_line(line)) for line in _input.splitlines())
